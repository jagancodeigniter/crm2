{* ********************************************************************************
* The content of this file is subject to the PDF Maker Free license.
* ("License"); You may not use this file except in compliance with the License
* The Initial Developer of the Original Code is IT-Solutions4You s.r.o.
* Portions created by IT-Solutions4You s.r.o. are Copyright(C) IT-Solutions4You s.r.o.
* All Rights Reserved.
* ****************************************************************************** *}

<div class="contents tabbable ui-sortable">


    <form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="index.php" enctype="multipart/form-data">
        <input type="hidden" name="module" value="PDFMaker">
        <input type="hidden" name="parenttab" value="{$PARENTTAB}">
        <input type="hidden" name="templateid" id="templateid" value="{$TEMPLATEID}">
        <input type="hidden" name="action" value="SavePDFTemplate">
        <input type="hidden" name="redirect" value="true">
        <input type="hidden" name="return_module" value="{$smarty.request.return_module}">
        <input type="hidden" name="return_view" value="{$smarty.request.return_view}">
        <input type="hidden" name="selectedTab" id="selectedTab" value="properties">
        <input type="hidden" name="selectedTab2" id="selectedTab2" value="body">

        <ul class="nav nav-tabs layoutTabs massEditTabs">
            <li class="detailviewTab active">
                <a data-toggle="tab" href="#pdfContentEdit" aria-expanded="true"><strong>{vtranslate('LBL_PROPERTIES_TAB',$MODULE)}</strong></a>
            </li>
            <li class="detailviewTab">
                <a data-toggle="tab" href="#pdfContentOther" aria-expanded="false"><strong>{vtranslate('LBL_OTHER_INFO',$MODULE)}</strong></a>
            </li>
            <li class="detailviewTab">
                <a data-toggle="tab" href="#pdfContentLabels" aria-expanded="false"><strong>{vtranslate('LBL_LABELS',$MODULE)}</strong></a>
            </li>
            <li class="detailviewTab">
                <a data-toggle="tab" href="#pdfContentProducts" aria-expanded="false"><strong>{vtranslate('LBL_ARTICLE',$MODULE)}</strong></a>
            </li>
            <li class="detailviewTab">
                <a data-toggle="tab" href="#pdfContentHeaderFooter" aria-expanded="false"><strong>{vtranslate('LBL_HEADER_TAB',$MODULE)} / {vtranslate('LBL_FOOTER_TAB',$MODULE)}</strong></a>
            </li>
            <li class="detailviewTab">
                <a data-toggle="tab" href="#editTabSettings" aria-expanded="false"><strong>{vtranslate('LBL_SETTINGS_TAB',$MODULE)}</strong></a>
            </li>
        </ul>
        <div >
            {********************************************* Settings DIV *************************************************}
            <div>
                <div class="row" >
                    <div class="left-block col-xs-4">
                        <div>
                            <div class="tab-content layoutContent themeTableColor overflowVisible">
                                <div class="tab-pane active" id="pdfContentEdit">
                                    <div class="edit-template-content col-lg-4" style="position:fixed;z-index:1000;">
                                        {********************************************* PROPERTIES DIV*************************************************}
                                        <table class="table no-border">
                                            <tbody id="properties_div">

                                            {* pdf source module and its available fields *}
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_MODULENAMES',$MODULE)} {$MODULENAME}:</label></td>
                                                <td class="fieldValue"><input type="hidden" name="modulename" id="modulename" value="{$SELECTMODULE}">

                                                    <select name="modulefields" id="modulefields" class="select2 col-sm-9">
                                                        <option value="">{vtranslate('LBL_SELECT_MODULE_FIELD',$MODULE)}</option>
                                                    </select>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-success InsertIntoTemplate" data-type="modulefields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="modulefields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            {* related modules and its fields *}
                                            <tr>
                                                <td class="fieldLabel" rowspan="2">
                                                    <label class="muted pull-right">{vtranslate('LBL_RELATED_MODULES',$MODULE)}:</label>
                                                </td>
                                                <td class="fieldValue">
                                                    <select name="relatedmodulesorce" id="relatedmodulesorce" class="select2 col-sm-12">
                                                        <option value="">{vtranslate('LBL_SELECT_MODULE',$MODULE)}</option>
                                                        {foreach item=RelMod from=$RELATED_MODULES}
                                                            <option value="{$RelMod.0}" data-module="{$RelMod.3}">{$RelMod.1} ({$RelMod.2})</option>
                                                        {/foreach}
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldValue">
                                                    <select name="relatedmodulefields" id="relatedmodulefields" class="select2 col-sm-9">
                                                        <option value="">{vtranslate('LBL_SELECT_MODULE_FIELD',$MODULE)}</option>
                                                    </select>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-success InsertIntoTemplate" data-type="relatedmodulefields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="relatedmodulefields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pdfContentOther">
                                    <dic class="edit-template-content col-lg-4" style="position:fixed;z-index:1000;">
                                        <table class="table no-border">
                                            {********************************************* Company and User information DIV *************************************************}
                                            <tbody>
                                            <tr>
                                                <td class="fieldLabel col-sm-4" rowspan="2"><label class="muted pull-right ">{vtranslate('LBL_COMPANY_USER_INFO',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="acc_info_type" id="acc_info_type" class="select2 col-sm-12">
                                                        {html_options  options=$CUI_BLOCKS}
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldValue">
                                                    <div id="acc_info_div" class="au_info_div" style="display:inline;">
                                                        <select name="acc_info" id="acc_info" class="select2 col-sm-9">
                                                            {html_options  options=$ACCOUNTINFORMATIONS}
                                                        </select>
                                                        <button type="button" class="btn btn-success InsertIntoTemplate" data-type="acc_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="acc_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                    <div id="user_info_div" class="au_info_div" style="display:none;">
                                                        <select name="user_info" id="user_info" class="select2 col-sm-9">
                                                            {html_options  options=$USERINFORMATIONS['a']}
                                                        </select>
                                                        <button type="button" class="btn btn-success InsertIntoTemplate" data-type="user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                    <div id="logged_user_info_div" class="au_info_div" style="display:none;">
                                                        <select name="logged_user_info" id="logged_user_info" class="select2 col-sm-9">
                                                            {html_options  options=$USERINFORMATIONS['l']}
                                                        </select>
                                                        <button type="button" class="btn btn- InsertIntoTemplate" data-type="logged_user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="logged_user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                    <div id="modifiedby_user_info_div" class="au_info_div" style="display:none;">
                                                        <select name="modifiedby_user_info" id="modifiedby_user_info" class="select2 col-sm-9">
                                                            {html_options  options=$USERINFORMATIONS['m']}
                                                        </select>
                                                        <button type="button" class="btn btn-success InsertIntoTemplate" data-type="modifiedby_user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="modifiedby_user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                    <div id="smcreator_user_info_div" class="au_info_div" style="display:none;">
                                                        <select name="smcreator_user_info" id="smcreator_user_info" class="select2 col-sm-9">
                                                            {html_options  options=$USERINFORMATIONS['c']}
                                                        </select>
                                                        <button type="button" class="btn btn-success InsertIntoTemplate" data-type="smcreator_user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                        <button type="button" class="btn btn-warning InsertLIntoTemplate" data-type="smcreator_user_info" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('TERMS_AND_CONDITIONS',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="invterandcon" id="invterandcon" class="select2 col-sm-9">
                                                        {html_options  options=$INVENTORYTERMSANDCONDITIONS}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="invterandcon" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_CURRENT_DATE',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="dateval" id="dateval" class="select2 col-sm-9">
                                                        {html_options  options=$DATE_VARS}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="dateval" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </dic>
                                </div>
                                <div class="tab-pane" id="pdfContentLabels">
                                    <dic class="edit-template-content col-lg-4" style="position:fixed;z-index:1000;">
                                        <table class="table no-border">
                                            {********************************************* Labels *************************************************}
                                            <tbody id="labels_div">
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_GLOBAL_LANG',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="global_lang" id="global_lang" class="select2 col-sm-10">
                                                        {html_options  options=$GLOBAL_LANG_LABELS}
                                                    </select>
                                                    <button type="button" class="btn btn-warning InsertIntoTemplate" data-type="global_lang" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_MODULE_LANG',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="module_lang" id="module_lang" class="select2 col-sm-10">
                                                        {html_options  options=$MODULE_LANG_LABELS}
                                                    </select>
                                                    <button type="button" class="btn btn-warning InsertIntoTemplate" data-type="module_lang" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-text-width"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </dic>
                                </div>
                                <div class="tab-pane" id="pdfContentProducts">
                                    <dic class="edit-template-content col-lg-4" style="position:fixed;z-index:1000;">
                                        <table class="table no-border">
                                            {*********************************************Products bloc DIV*************************************************}
                                            <tbody id="products_div">
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_PRODUCT_BLOC_TPL',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="productbloctpl2" id="productbloctpl2" class="select2 col-sm-10">
                                                        {html_options  options=$PRODUCT_BLOC_TPL}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="productbloctpl2" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_ARTICLE',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="articelvar" id="articelvar" class="select2 col-sm-10">
                                                        {html_options  options=$ARTICLE_STRINGS}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="articelvar" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            {* insert products & services fields into text *}
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">*{vtranslate('LBL_PRODUCTS_AVLBL',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="psfields" id="psfields" class="select2 col-sm-10">
                                                        {html_options  options=$SELECT_PRODUCT_FIELD}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="psfields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            {* products fields *}
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">*{vtranslate('LBL_PRODUCTS_FIELDS',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="productfields" id="productfields" class="select2 col-sm-10">
                                                        {html_options  options=$PRODUCTS_FIELDS}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="productfields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            {* services fields *}
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">*{vtranslate('LBL_SERVICES_FIELDS',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="servicesfields" id="servicesfields" class="select2 col-sm-10">
                                                        {html_options  options=$SERVICES_FIELDS}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="servicesfields" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel" colspan="4"><label class="muted"><small>{vtranslate('LBL_PRODUCT_FIELD_INFO',$MODULE)}</small></label></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </dic>
                                </div>
                                <div class="tab-pane" id="pdfContentHeaderFooter">
                                    <dic class="edit-template-content col-lg-4" style="position:fixed;z-index:1000;">
                                        <table class="table no-border">
                                            {********************************************* Header/Footer *************************************************}
                                            <tbody id="headerfooter_div">
                                            {* pdf header variables*}
                                            <tr id="header_variables">
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_HEADER_FOOTER_VARIABLES',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <select name="header_var" id="header_var" class="select2 col-sm-8">
                                                        {html_options  options=$HEAD_FOOT_VARS selected=""}
                                                    </select>
                                                    <button type="button" class="btn btn-success InsertIntoTemplate" data-type="header_var" title="{vtranslate('LBL_INSERT_TO_TEXT',$MODULE)}"><i class="fa fa-usd"></i></button>
                                                </td>
                                            </tr>
                                            {* don't display header on first page *}
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_DISPLAY_HEADER',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <b>{vtranslate('LBL_ALL_PAGES',$MODULE)}</b>&nbsp;<input type="checkbox" id="dh_allid" name="dh_all" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'header');" {$DH_ALL}/>
                                                    &nbsp;&nbsp;
                                                    {vtranslate('LBL_FIRST_PAGE',$MODULE)}&nbsp;<input type="checkbox" id="dh_firstid" name="dh_first" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'header');" {$DH_FIRST}/>
                                                    &nbsp;&nbsp;
                                                    {vtranslate('LBL_OTHER_PAGES',$MODULE)}&nbsp;<input type="checkbox" id="dh_otherid" name="dh_other" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'header');" {$DH_OTHER}/>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_DISPLAY_FOOTER',$MODULE)}:</label></td>
                                                <td class="fieldValue">
                                                    <b>{vtranslate('LBL_ALL_PAGES',$MODULE)}</b>&nbsp;<input type="checkbox" id="df_allid" name="df_all" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'footer');" {$DF_ALL}/>
                                                    &nbsp;&nbsp;
                                                    {vtranslate('LBL_FIRST_PAGE',$MODULE)}&nbsp;<input type="checkbox" id="df_firstid" name="df_first" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'footer');" {$DF_FIRST}/>
                                                    &nbsp;&nbsp;
                                                    {vtranslate('LBL_OTHER_PAGES',$MODULE)}&nbsp;<input type="checkbox" id="df_otherid" name="df_other" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'footer');" {$DF_OTHER}/>
                                                    &nbsp;&nbsp;
                                                    {vtranslate('LBL_LAST_PAGE',$MODULE)}&nbsp;<input type="checkbox" id="df_lastid" name="df_last" onclick="PDFMaker_EditJs.hf_checkboxes_changed(this, 'footer');" {$DF_LAST}/>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </dic>
                                </div>
                                <div class="tab-pane" id="editTabSettings">
                                    <table class="table no-border">
                                        <tbody id="settings_div">
                                        <tr>
                                            <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_DESCRIPTION',$MODULE)}:</label></td>
                                            <td class="fieldValue"><input name="description" type="text" value="{$DESCRIPTION}" class="inputElement" tabindex="2"></td>
                                        </tr>
                                        {* pdf format settings *}
                                        <tr>
                                            <td class="fieldLabel">
                                                <label class="muted pull-right">{vtranslate('LBL_PDF_FORMAT',$MODULE)}:</label>
                                            </td>
                                            <td class="fieldValue">
                                                <select name="pdf_format" id="pdf_format" class="select2 col-sm-12" onchange="PDFMaker_EditJs.CustomFormat();">
                                                    {html_options  options=$FORMATS selected=$SELECT_FORMAT}
                                                </select>
                                                <table class="table showInlineTable" id="custom_format_table" {if $SELECT_FORMAT neq 'Custom'}style="display:none"{/if}>
                                                    <tr>
                                                        <td align="right" nowrap>{vtranslate('LBL_WIDTH',$MODULE)}</td>
                                                        <td>
                                                            <input type="text" name="pdf_format_width" id="pdf_format_width" class="detailedViewTextBox" value="{$CUSTOM_FORMAT.width}" style="width:50px">
                                                        </td>
                                                        <td align="right" nowrap>{vtranslate('LBL_HEIGHT',$MODULE)}</td>
                                                        <td>
                                                            <input type="text" name="pdf_format_height" id="pdf_format_height" class="detailedViewTextBox" value="{$CUSTOM_FORMAT.height}" style="width:50px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        {* pdf orientation settings *}
                                        <tr>
                                            <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_PDF_ORIENTATION',$MODULE)}:</label></td>
                                            <td class="fieldValue">
                                                <select name="pdf_orientation" id="pdf_orientation" class="select2 col-sm-12">
                                                    {html_options  options=$ORIENTATIONS selected=$SELECT_ORIENTATION}
                                                </select>
                                            </td>
                                        </tr>
                                        {* ignored picklist values settings *}
                                        <tr>
                                            <td class="fieldLabel" title="{vtranslate('LBL_IGNORE_PICKLIST_VALUES_DESC',$MODULE)}"><label class="muted pull-right">{vtranslate('LBL_IGNORE_PICKLIST_VALUES',$MODULE)}:</label></td>
                                            <td class="fieldValue" title="{vtranslate('LBL_IGNORE_PICKLIST_VALUES_DESC',$MODULE)}"><input type="text" name="ignore_picklist_values" value="{$IGNORE_PICKLIST_VALUES}" class="inputElement"/></td>
                                        </tr>
                                        {* pdf margin settings *}
                                        {assign var=margin_input_width value='50px'}
                                        {assign var=margin_label_width value='50px'}
                                        <tr>
                                            <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_MARGINS',$MODULE)}:</label></td>
                                            <td class="fieldValue">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td align="right" nowrap>{vtranslate('LBL_TOP',$MODULE)}</td>
                                                        <td>
                                                            <input type="text" name="margin_top" id="margin_top" class="inputElement" value="{$MARGINS.top}" style="width:{$margin_input_width}" onKeyUp="PDFMaker_EditJs.ControlNumber('margin_top', false);">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" nowrap>{vtranslate('LBL_BOTTOM',$MODULE)}</td>
                                                        <td>
                                                            <input type="text" name="margin_bottom" id="margin_bottom" class="inputElement" value="{$MARGINS.bottom}" style="width:{$margin_input_width}" onKeyUp="PDFMaker_EditJs.ControlNumber('margin_bottom', false);">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td align="right" nowrap>{vtranslate('LBL_LEFT',$MODULE)}</td>
                                                        <td>
                                                            <input type="text" name="margin_left"  id="margin_left" class="inputElement" value="{$MARGINS.left}" style="width:{$margin_input_width}" onKeyUp="PDFMaker_EditJs.ControlNumber('margin_left', false);">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td align="right" nowrap>{vtranslate('LBL_RIGHT',$MODULE)}</td>
                                                        <td>
                                                            <input type="text" name="margin_right" id="margin_right" class="inputElement" value="{$MARGINS.right}" style="width:{$margin_input_width}" onKeyUp="PDFMaker_EditJs.ControlNumber('margin_right', false);">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        {* decimal settings *}
                                        <tr>
                                            <td class="fieldLabel"><label class="muted pull-right">{vtranslate('LBL_DECIMALS',$MODULE)}:</label></td>
                                            <td class="fieldValue">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td align="right" nowrap>{vtranslate('LBL_DEC_POINT',$MODULE)}</td>
                                                        <td><input type="text" maxlength="2" name="dec_point" class="inputElement" value="{$DECIMALS.point}" style="width:{$margin_input_width}"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" nowrap>{vtranslate('LBL_DEC_DECIMALS',$MODULE)}</td>
                                                        <td><input type="text" maxlength="2" name="dec_decimals" class="inputElement" value="{$DECIMALS.decimals}" style="width:{$margin_input_width}"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" nowrap>{vtranslate('LBL_DEC_THOUSANDS',$MODULE)}</td>
                                                        <td><input type="text" maxlength="2" name="dec_thousands" class="inputElement" value="{$DECIMALS.thousands}" style="width:{$margin_input_width}"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                   {************************************** END OF TABS BLOCK *************************************}
                    <div class="middle-block col-xs-8">
                        <div id="ContentEditorTabs">
                            <ul class="nav nav-pills">
                                <li class="active" data-type="body">
                                    <a href="#body_div2" aria-expanded="false" data-toggle="tab">{vtranslate('LBL_BODY',$MODULE)}</a>
                                </li>
                                <li data-type="header">
                                    <a href="#header_div2" aria-expanded="false" data-toggle="tab">{vtranslate('LBL_HEADER_TAB',$MODULE)}</a>
                                </li>
                                <li data-type="footer">
                                    <a href="#footer_div2" aria-expanded="false" data-toggle="tab">{vtranslate('LBL_FOOTER_TAB',$MODULE)}</a>
                                </li>
                            </ul>
                        </div>
                        {*********************************************BODY DIV*************************************************}
                        <div class="tab-content ">
                            <div class="tab-pane active" id="body_div2">
                                <textarea name="body" id="body" style="width:90%;height:700px" class=small tabindex="5">{$BODY}</textarea>
                            </div>
                            {*********************************************Header DIV*************************************************}
                            <div class="tab-pane" id="header_div2">
                                <textarea name="header_body" id="header_body" style="width:90%;height:200px" class="small">{$HEADER}</textarea>
                            </div>
                            {*********************************************Footer DIV*************************************************}
                            <div class="tab-pane" id="footer_div2">
                                <textarea name="footer_body" id="footer_body" style="width:90%;height:200px" class="small">{$FOOTER}</textarea>
                            </div>
                        </div>
                        <script type="text/javascript">
                            {literal} jQuery(document).ready(function(){
                                CKEDITOR.replace('body', {height: '1000'});
                                CKEDITOR.replace('header_body', {height: '1000'});
                                CKEDITOR.replace('footer_body', {height: '1000'});
                            }){/literal}
                        </script>
                    </div>
                </div>
            </div>
        </div>
        {*literal} <script type="text/javascript" src="modules/PDFMaker/fck_config.js"></script>{/literal*}

        <div class="modal-overlay-footer row-fluid">
            <div class="textAlignCenter ">
                <button class="btn" type="submit" onclick="document.EditView.redirect.value = 'false';" ><strong>{vtranslate('LBL_APPLY',$MODULE)}</strong></button>&nbsp;&nbsp;
                <button class="btn btn-success" type="submit" ><strong>{vtranslate('LBL_SAVE', $MODULE)}</strong></button>
                {if $smarty.request.return_view neq ''}
                    <a class="cancelLink" type="reset" onclick="window.location.href = 'index.php?module={if $smarty.request.return_module neq ''}{$smarty.request.return_module}{else}PDFMaker{/if}&view={$smarty.request.return_view}{if $smarty.request.templateid neq ""  && $smarty.request.return_view neq "List"}&templateid={$smarty.request.templateid}{/if}';">{vtranslate('LBL_CANCEL', $MODULE)}</a>
                {else}
                    <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">{vtranslate('LBL_CANCEL', $MODULE)}</a>
                {/if}            			
            </div>
            <div align="center" class="small" style="color: rgb(153, 153, 153);">{vtranslate('PDF_MAKER',$MODULE)} {$VERSION} {vtranslate('COPYRIGHT',$MODULE)}</div>
        </div>
    </form>
</div>
<div id="company_stamp_signature_content" class="hide">
    {$COMPANY_STAMP_SIGNATURE}
</div>
<div id="companylogo_content" class="hide">
    {$COMPANYLOGO}
</div>
<div id="company_header_signature_content" class="hide">
    {$COMPANY_HEADER_SIGNATURE}
</div>
<div id="vatblock_table_content" class="hide">
    {$VATBLOCK_TABLE}
</div>



<script type="text/javascript">

    var selectedTab = 'properties';
    var selectedTab2 = 'body';
    var module_blocks = new Array();
 
    var selected_module = '{$SELECTMODULE}';

    var constructedOptionValue;
    var constructedOptionName;

    jQuery(document).ready(function() {

        jQuery.fn.scrollBottom = function() {
            return jQuery(document).height() - this.scrollTop() - this.height();
        };

        var $el = jQuery('.edit-template-content');
        var $window = jQuery(window);
        var top = 127;

        $window.bind("scroll resize", function() {

            var gap = $window.height() - $el.height() - 20;
            var scrollTop = $window.scrollTop();

            if (scrollTop < top - 125) {
                $el.css({
                    top: (top - scrollTop) + "px",
                    bottom: "auto"
                });
            } else {
                $el.css({
                    top: top  + "px",
                    bottom: "auto"
                });
            }
        }).scroll();
    });

</script>

